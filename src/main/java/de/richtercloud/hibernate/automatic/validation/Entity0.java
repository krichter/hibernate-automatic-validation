package de.richtercloud.hibernate.automatic.validation;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Entity0 implements Serializable {
    @Id
    private Long id;
    @Basic
    @NotNull
    private String property0;

    public Entity0() {
    }

    public Entity0(Long id,
            String property0) {
        this.id = id;
        this.property0 = property0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProperty0() {
        return property0;
    }

    public void setProperty0(String property0) {
        this.property0 = property0;
    }
}
