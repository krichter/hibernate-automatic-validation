package de.richtercloud.hibernate.automatic.validation;

import java.sql.SQLException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

    public static void main(String[] args) throws SQLException {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("de.richtercloud_hibernate-automatic-validation_jar_1.0-SNAPSHOTPU");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Entity0 entity0 = new Entity0(1l, "abc");
        entityManager.getTransaction().begin();
        entityManager.persist(entity0);
        entityManager.flush();
        entityManager.getTransaction().commit();
        entityManager.close();
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entity0 = entityManager.merge(entity0);
        entity0.setProperty0(null);
        entityManager.merge(entity0);
        entityManager.flush();
        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }
}
